#include <iostream>
#include <string.h>
#include <vector>
#include <fstream>
#ifndef TA_LIBC_H
    #include "ta_libc.h"
#endif
#include "csv_parser.hpp"
#include "main.hpp"


ofstream out;


void print_result(const string &datetime,
                  const TA_Real &profit,
                  const unsigned short &fastN,
                  const unsigned short &idFastMa,
                  const unsigned short &slowN,
                  const unsigned short &idSlowMa,
                  const unsigned short &timeframe,
                  const strategies &strategy)
{
    out << datetime << ",";
    out << profit << ",";
    out << fastN << ",";
    out << idFastMa << ",";
    out << slowN << ",";
    out << idSlowMa << ",";
    out << timeframe << ",";
    out << strategy << endl;
}


void revers_transaction(TA_Real &reversPrice,
                        TA_Real &currPrice,
                        directs &currDirect,
                        TA_Real &profit) {
    if (reversPrice != 0) {
        if (currDirect == UP) {
            profit += reversPrice - currPrice - 2 * COMISSION;
        } else {
            profit += currPrice - reversPrice - 2 * COMISSION;
        }
    }
    reversPrice = currPrice;
}


void long_transaction(TA_Real &longPrice,
                      TA_Real &currPrice,
                      directs &currDirect,
                      TA_Real &profit) {
    if (currDirect == UP) {
        longPrice = currPrice;
    } else if (longPrice > 0) {
        profit += currPrice - longPrice - 2 * COMISSION;
    }
}


void short_transaction(TA_Real &shortPrice,
                       TA_Real &currPrice,
                       directs &currDirect,
                       TA_Real &profit) {
    if (currDirect == DOWN) {
        shortPrice = currPrice;
    } else if (shortPrice > 0) {
        profit += shortPrice - currPrice - 2 * COMISSION;
    }
}


void call(const vector <TA_Real> &prices,
          const maInfoStruct &maInfoFast,
          const maInfoStruct &maInfoSlow,
          const vector <string> &datetimes,
          const unsigned short &timeframe)
{
    /* инициализация смотрим с какой промежуток по какой будем вести отсчет */
    TA_Integer startPrices = maInfoSlow.outBeg; // как будет отсчитываться цена + время
    TA_Integer indexFastMa = maInfoSlow.outBeg - maInfoFast.outBeg;
    TA_Integer indexSlowMa = 0;
    if (startPrices < maInfoFast.outBeg) {
        startPrices = maInfoFast.outBeg;
        indexFastMa = 0;
        indexSlowMa = maInfoFast.outBeg - maInfoSlow.outBeg;
    }
    TA_Integer endPrices = startPrices - 1;
    if (maInfoSlow.outNbElement > maInfoFast.outNbElement) {
        endPrices += maInfoFast.outNbElement;
    } else {
        endPrices += maInfoSlow.outNbElement;
    }
    directs prevDirect = UNDEFINED;
    directs currDirect = UNDEFINED;
    TA_Real currPrice;

    TA_Real maValueFast;
    TA_Real maValueSlow;
    TA_Real shortPrice = 0; // цена при покупки-продажи для короткой стратегии
    TA_Real longPrice = 0; // цена при покупки-продажи для длинной стратегии
    TA_Real reversPrice = 0; // цена при покупки-продажи для реверсивной стратегии
    TA_Real shortProfit = 0;
    TA_Real longProfit = 0;
    TA_Real reversProfit = 0;
    string currDay = datetimes[startPrices].substr(0, 10);
    string currDate;
    for (TA_Integer priceIndex=startPrices; priceIndex<endPrices; priceIndex++) {
        currDate = datetimes[priceIndex].substr(0, 10);
        if (currDay.compare(currDate) < 0) {
            // по окончанию дня продаем все активы
            revers_transaction(reversPrice, currPrice, currDirect, reversProfit);
            long_transaction(longPrice, currPrice, currDirect, longProfit);
            short_transaction(shortPrice, currPrice, currDirect, shortProfit);
            // выводим результат на экран
            print_result(currDay,
                         reversProfit,
                         maInfoFast.n,
                         maInfoFast.idMa,
                         maInfoSlow.n,
                         maInfoSlow.idMa,
                         timeframe,
                         REVERS);
            print_result(currDay,
                         longProfit,
                         maInfoFast.n,
                         maInfoFast.idMa,
                         maInfoSlow.n,
                         maInfoSlow.idMa,
                         timeframe,
                         LONG);
            print_result(currDay,
                         shortProfit,
                         maInfoFast.n,
                         maInfoFast.idMa,
                         maInfoSlow.n,
                         maInfoSlow.idMa,
                         timeframe,
                         SHORT);
            // обнуляем все
            shortProfit = 0;
            longProfit = 0;
            reversProfit = 0;
            currDay = currDate;
            prevDirect = UNDEFINED;
            currDirect = UNDEFINED;
        }
        currPrice = prices[priceIndex];
        maValueFast = maInfoFast.result[indexFastMa];
        maValueSlow = maInfoSlow.result[indexSlowMa];
        if (maValueFast > maValueSlow) {
            currDirect = UP;
        } else if (maValueFast < maValueSlow) {
            currDirect = DOWN;
        } else {
            continue;
        }
        if (currDirect != prevDirect) {
            // реверсивная стратегия
            revers_transaction(reversPrice, currPrice, currDirect, reversProfit);
            // длинная стратегия
            long_transaction(longPrice, currPrice, currDirect, longProfit);
            // короткая стратегия
            short_transaction(shortPrice, currPrice, currDirect, shortProfit);
        }
        prevDirect = currDirect;
        indexFastMa++;
        indexSlowMa++;
    }
}


void get_ma_values(const vector <TA_Real> &prices,
                   const vector <string> &datetimes,
                   const unsigned short &timeframe)
{
    //TODO: сделать единократный расчет скользящих средних, чтобы не считать их каждый раз
    using namespace std;
    unsigned int endPrice = prices.size();
    TA_Real tmpArray[endPrice];
    maInfoStruct maInfo[COUNT_MA_VALUES];
    TA_RetCode retCode;
    unsigned int endMaCall = 0;
    /* расчитываем заранее скользящие средние */
    TA_Integer outNbElement = 0;
    TA_Integer outBeg = 0;
    for (unsigned short func_num=0; func_num<COUNT_MA; func_num++) {
        for (unsigned short n=N_MIN; n<=N_MAX; n++) {
            retCode = standartFuncs[func_num](0,
                                    endPrice,
                                    &prices[0],
                                    n,
                                    &outBeg,
                                    &outNbElement,
                                    &tmpArray[0]);
            if(retCode != TA_SUCCESS) {
                continue;
            }
            maInfo[endMaCall].outBeg = outBeg;
            maInfo[endMaCall].outNbElement = outNbElement;
            maInfo[endMaCall].n = n;
            maInfo[endMaCall].idMa = func_num;
            for (TA_Integer pos=0; pos<maInfo[endMaCall].outNbElement; pos++) {
                maInfo[endMaCall].result.push_back(tmpArray[pos]);
            }
            endMaCall++;
        }
    }
    for (unsigned short fastIndex=0; fastIndex<endMaCall; fastIndex++) {
        for (unsigned short slowIndex=0; slowIndex<endMaCall; slowIndex++) {
            if (maInfo[slowIndex].n > maInfo[fastIndex].n) {
                call(prices,
                     maInfo[fastIndex],
                     maInfo[slowIndex],
                     datetimes,
                     timeframe);
            }
        }
    }
/* для будущих поколений 
    TA_MAType funcs[] = {TA_MAType_SMA, TA_MAType_EMA, TA_MAType_WMA,
                         TA_MAType_DEMA, TA_MAType_TEMA, TA_MAType_TRIMA,
                         TA_MAType_KAMA, TA_MAType_MAMA, TA_MAType_T3};
    */
}


int main(int argc, char *argv[])
{
    if (argc != 2) {
        std::cout << "Нужно указать название бумаги\n";
        return 1;
    }
    char output_filename_tmp[] = "others/%s_full.csv";
    char output_filename[50];
    sprintf(output_filename, output_filename_tmp, argv[1]);
    out.open(output_filename);
    TA_RetCode retCode = TA_Initialize();
    if(retCode != TA_SUCCESS) {
        printf("Cannot initialize TA-Lib (%d)!\n", retCode);
        return 1;
    }
    standartFuncs[0] = TA_SMA;
    standartFuncs[1] = TA_WMA;
    standartFuncs[2] = TA_EMA;
    standartFuncs[3] = TA_DEMA;
    standartFuncs[4] = TA_TEMA;
    standartFuncs[5] = TA_TRIMA;
    standartFuncs[6] = TA_KAMA;

    csv_parser file_parser;
    char template_filename[50] = "data/";
    strcat(template_filename, argv[1]);
    strcat(template_filename, "_%d.csv");

    char filename[30];
    vector <TA_Real> prices;
    vector <string> datetimes;
    for (unsigned short timeframe: TIMEFRAMES) {
        sprintf(filename, template_filename, timeframe);
        file_parser.init(filename);

        while (file_parser.has_more_rows()) {
            csv_row row = file_parser.get_row();
            prices.push_back(std::stoi(row[1].c_str()));
            datetimes.push_back(row[0]);
        }
        get_ma_values(prices, datetimes, timeframe);
        prices.clear();
        datetimes.clear();
    }

    TA_Shutdown();
    return 0;
}
