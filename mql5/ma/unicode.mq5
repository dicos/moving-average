//+------------------------------------------------------------------+
//|                                                      unicode.mq5 |
//|                        Copyright 2014, MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property library
#property link      "http://trading.aurorasolutions.org/mql5-c-communication/"

/// <summary>
/// Converts the provided ANSI string to a Unicode format string
/// </summary>
/// <param name=" ansiString ">ANSI string</param>
string ANSIToUnicode(string ansiString)
{
     ushort character;
     long intRemainder;
     long intNextCharacter;
     double dblRemainder;
     double nextCharacter;
     string strRemainder;
     string result = "";
     int length = StringLen(ansiString);
     if (length > 0)
     {
          for (int i=0; i<StringLen(ansiString); i++)
          {
               string intermediateString="   ";
               character = ushort(StringGetCharacter(ansiString, i));
               dblRemainder = MathMod(character, 256);
               strRemainder = DoubleToString(dblRemainder, 0);
               intRemainder = StringToInteger(strRemainder);
               nextCharacter = (character - intRemainder) / 256;
               strRemainder = DoubleToString(nextCharacter, 0);
               intNextCharacter = StringToInteger(strRemainder);
               if (intRemainder != 0)
               {
                    StringSetCharacter(intermediateString, 0, ushort(intRemainder));
                    StringSetCharacter(intermediateString, 1, ushort(intNextCharacter));
                    StringConcatenate(result, result, intermediateString);
               }
               else
               {
                    break;
               }
          }
     }
     return(result);
}
 
/// <summary>
/// Converts the provided Unicode string to a ANSI format string
/// </summary>
/// <param name=" unicodeString ">Unicode string</param>
string Unicode2ANSI(string unicodeString)
{
     int length = StringLen(unicodeString);
     int position;
     uchar currentSingleByteChar;
     uchar nextSingleByteChar;
     ulong intermediateCharacter;
     string intermediateString;
     string result = "";
 
     if (length != 0)
     {
          intermediateString = " ";
          position = 0;
          while (position < length)
          {
               currentSingleByteChar = uchar(StringGetCharacter(unicodeString, position));
               if (position + 1 < length)
               {
                    nextSingleByteChar = uchar(StringGetCharacter(unicodeString, position + 1));
               }
               else
               {
                    nextSingleByteChar = 0;
               }
               intermediateCharacter = nextSingleByteChar * 256 + currentSingleByteChar;
               StringSetCharacter(intermediateString, 0, ushort(intermediateCharacter));
               StringConcatenate(result, result, intermediateString);
               position = position + 2;
          }
     }
     return(result);
}
