#property copyright "Dmitry Kosolapov <self@dicos.ru>"
#property link      ""
#property version   "1.0"

input double comission;
input double lot = 1.0;
input bool showChart = false;

#include "..\Projects\unicode.mq5"

#import "..\Projects\out.dll"
string get_best(int &prices[], int count, int comission);
int call_func(int n, int idMa, int &prices[], int count);
#import

enum STRATEGIES_ENUM {LONG, SHORT, REVERS, END_STRATEGIES};
enum LOT_ENUM {UNDEFINED, BUY, SELL};


struct MaParamsStruct {
    ushort n;
    ushort idMa;
};
struct BestStruct {
    MaParamsStruct fast;
    MaParamsStruct slow;
    STRATEGIES_ENUM strategy;
    int profit;
    ENUM_TIMEFRAMES timeframe;
};

const int FACTOR = 100000;

MqlDateTime prevCallBest;

//const ENUM_TIMEFRAMES TIMEFRAMES[] = {PERIOD_M5, PERIOD_M10,
//                                      PERIOD_M15, PERIOD_M20,};
const ENUM_TIMEFRAMES TIMEFRAMES[] = {PERIOD_M20};
const int COUNT_BARS = 600;

MqlTradeRequest req;
MqlTradeResult result;
MqlTradeCheckResult check;
BestStruct best;
LOT_ENUM currentLot = UNDEFINED;


BestStruct getBest()
{
   best.profit = -1;  // ������ �����-�� ������������� �����
   MqlRates rates[];
   ArraySetAsSeries(rates, true);
   int intComission = (int) (MathRound(comission) * 100);
   
   for (int i_tf=0; i_tf<ArraySize(TIMEFRAMES); i_tf++) {
      ENUM_TIMEFRAMES timeframe = TIMEFRAMES[i_tf];
      int copied = 0;
      copied = CopyRates(Symbol(),
                         timeframe,
                         0,
                         4200,
                         rates);
      if (copied > 0) {
         int prices[];
         int end = copied - 1;
         ArrayResize(prices, copied);
         for(int i=0; i<copied; i++) {
            prices[i] = (int) (rates[end-i].close * FACTOR);
         }
         string result_str = ANSIToUnicode(get_best(prices,
                                                    ArraySize(prices),
                                                    intComission));
         Print(result_str);
         StringReplace(result_str, " ", "");
         string parametrs_str[];
         int count_params = StringSplit(result_str, ';', parametrs_str);
         if (count_params == 6) {
            int profit = StringToInteger(parametrs_str[5]) / FACTOR;
            if (profit > best.profit) {
               best.profit = profit;
               best.fast.n = (ushort) StringToInteger(parametrs_str[0]);
               best.fast.idMa = (ushort) StringToInteger(parametrs_str[1]);
               best.slow.n = (ushort) StringToInteger(parametrs_str[2]);
               best.slow.idMa = (ushort) StringToInteger(parametrs_str[3]);
               best.strategy = (ushort) StringToInteger(parametrs_str[4]);
               best.timeframe = timeframe;
            }
         }
      }
   }
   Print ("Fast: ", best.fast.n, " ", best.fast.idMa);
   Print ("Slow: ", best.slow.n, " ", best.slow.idMa);
   Print("Strategy:" ,best.strategy);
   return best;
}

void transaction(double lot_count, ENUM_ORDER_TYPE orderType)
{
   req.type=orderType;
   req.action=TRADE_ACTION_DEAL;
   req.symbol = _Symbol;
   req.volume = lot_count;
   req.type_filling=ORDER_FILLING_FOK;
   if(OrderCheck(req,check)){
      if(!OrderSend(req,result) || result.deal==0) {
         //Print("OrderSend Code: ",result.retcode);
      }
   } else {
      string action = "";
      if (orderType == ORDER_TYPE_BUY) {
         action = "�������";
      } else {
         action = "Sell";
      }
      Print("������: ", GetLastError(), " �����������: ", check.comment, " ������: ", check.balance, " -- ", action);
      
   }
}


void closeOrders()
{
   if (currentLot == SELL) {
      transaction(lot, ORDER_TYPE_BUY);
   } else if (currentLot == BUY) {
      transaction(lot, ORDER_TYPE_SELL);
   }
   currentLot = UNDEFINED;
}


int OnInit()
{
   ChartSetInteger(ChartID(), CHART_MODE, CHART_CANDLES);
   ChartSetSymbolPeriod(ChartID(), _Symbol, getBest().timeframe);
   TimeToStruct(TimeTradeServer(), prevCallBest);
   return INIT_SUCCEEDED;
  }


void OnDeinit(const int reason)
{
   if (reason != REASON_CHARTCHANGE) {
      closeOrders();
   }
}


void OnTick()
  {
   if(Bars(_Symbol, best.timeframe) < COUNT_BARS) {
      Alert("�� ������� ������ ", COUNT_BARS, " �����, �������� �� ����� ��������!!");
      return;
   }

//--- ��� ���������� �������� ������� ���� �� ���������� static-���������� Old_Time.
//--- ��� ������ ���������� ������� OnTick �� ����� ���������� ����� �������� ���� � ����������� ��������.
//--- ���� ��� �� �����, ��� ��������, ��� ����� �������� ����� ���.

   static datetime oldTime;
   datetime newTime[2];
   bool isNewBar = false;
   static bool isEntry = false;

//--- �������� ����� �������� ���� � ������� New_Time[0]
   int copied = CopyTime(_Symbol, best.timeframe, 0, 2, newTime);
   if(copied > 0) {
      if(oldTime!=newTime[0]) {
         isNewBar=true;   // ����� ���
         if(MQL5InfoInteger(MQL5_DEBUGGING)) {
            Print("����� ���", newTime[0], "������ ���", oldTime);
         }
         oldTime=newTime[0];   // ��������� ����� ����
        }
   } else {
      Alert("������ ����������� �������, ����� ������ =",GetLastError());
      ResetLastError();
      return;
   }
   if (! isNewBar) {
      return;
   }
   MqlRates rates[];
   ArraySetAsSeries(rates, true);
   copied = CopyRates(Symbol(),
                      best.timeframe,
                      0,
                      COUNT_BARS,
                      rates);
   if (copied > 0) {
      int prices[];
      ArrayResize(prices, copied);
      int end = copied - 1;
      for(int i=0; i<copied; i++) {
         prices[i] = (int) (rates[end-i].close * FACTOR);
      }
      MqlDateTime currCallBest;
      TimeToStruct(TimeTradeServer(), currCallBest);
      if (currCallBest.day != prevCallBest.day) {
         getBest();
         prevCallBest = currCallBest;
         //closeOrders();
      }
      int fastMa = call_func(best.fast.n, best.fast.idMa, prices, copied);
      int slowMa = call_func(best.slow.n, best.slow.idMa, prices, copied);

      if (slowMa <= 0 || fastMa <= 0) {
         //Print("--bar date:", TimeToString(oldTime)," date: ", TimeToString(rates[copied-1].time), " rate: ", rates[copied-1].close, " currPrice:", prices[copied-1], " fastMa: ", fastMa, " slowMa: ", slowMa);
         return;
      }
      
      double normalizeFast = ((double) fastMa ) / FACTOR;
      double normalizeSlow = ((double) slowMa) / FACTOR;
      if (showChart) {
         string name_salt = IntegerToString(newTime[0]);
         string fastName = "fastMa" + name_salt;
         double static prevNormalizeFast = 0;
         if (prevNormalizeFast != 0) {
            ObjectCreate(0, fastName, OBJ_TREND, 0, newTime[1], prevNormalizeFast, newTime[0], normalizeFast);
            ObjectSetInteger(0, fastName, OBJPROP_COLOR, clrRed);
         }
         prevNormalizeFast = normalizeFast;
         
         string slowName = "slowMa" + name_salt;
         double static prevNormalizeSlow = 0;
         if (prevNormalizeSlow != 0) {
            ObjectCreate(0, slowName, OBJ_TREND, 0, newTime[1], prevNormalizeSlow, newTime[0], normalizeSlow);
            ObjectSetInteger(0, slowName, OBJPROP_COLOR, clrBlue);
         }
         prevNormalizeSlow = normalizeSlow;
      }
      //Print("bar date:", TimeToString(oldTime)," date: ", TimeToString(rates[copied-1].time), " rate: ", rates[copied-1].close, " currPrice:", prices[copied-1], " fastMa: ", normalizeFast, " slowMa: ", normalizeSlow);
      //return;
      
      
      LOT_ENUM nextLot = UNDEFINED;
      if (fastMa > slowMa) {
         nextLot = BUY;
      } else if (fastMa < slowMa) {
         nextLot = SELL;
      }
      
      double lotVolume = lot;
      if (!isEntry) {
         isEntry = true;
         return;
      }
      if (currentLot != UNDEFINED && best.strategy == REVERS) {
         lotVolume *= 2;
      }
      if (nextLot != currentLot && nextLot != UNDEFINED) {
         if (nextLot == BUY) {
            transaction(lotVolume, ORDER_TYPE_BUY);
            //Print("currPrice:", prices[0], " fastMa: ", normalizeFast, " slowMa: ", normalizeSlow, " Action: buy");
         } else if (nextLot == SELL) {
            transaction(lotVolume, ORDER_TYPE_SELL);
            //Print("currPrice:", prices[0], " fastMa: ", normalizeFast, " slowMa: ", normalizeSlow, " Action: sell");
         }
         currentLot = nextLot;
         
      }
   }
   return;
  }
