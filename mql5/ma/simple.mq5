input double comission;

#include "..\Projects\unicode.mq5"

#import "..\Projects\out.dll"
string get_best(int &prices[], int count, int comission);
#import


struct MaParamsStruct {
    ushort n;
    ushort idMa;
};


struct BestStruct {
    MaParamsStruct fast;
    MaParamsStruct slow;
    ushort strategy;
    double profit;
    ENUM_TIMEFRAMES timeframe;
};

int intComission = ((int) MathRound(comission) * 100);
const int PRICE_RATIO = 1000;


const ENUM_TIMEFRAMES TIMEFRAMES[] = {PERIOD_M5, PERIOD_M10,
                                      PERIOD_M15, PERIOD_M20,};


void OnStart()
{
   BestStruct indicatorParams;
   indicatorParams.profit = -1000;  // ������ �����-�� ������������� �����
   uint oneDay = 60 * 60 * 60 * 24;
   datetime currentDate = TimeLocal();
   MqlDateTime currentDay;
   TimeToStruct(currentDate, currentDay);
   currentDay.sec = 0;
   currentDay.min = 0;
   currentDay.hour = 0;
   currentDate = StructToTime(currentDay);
   datetime endDate = currentDate - oneDay;
   datetime startDate = currentDate - (oneDay * 2);
   MqlRates rates[];
   ArraySetAsSeries(rates, true);
   
   for (int i_tf=0; i_tf<ArraySize(TIMEFRAMES); i_tf++) {
      ENUM_TIMEFRAMES timeframe = TIMEFRAMES[i_tf];
      int copied = 0;
      copied = CopyRates(Symbol(),
                         timeframe,
                         startDate,
                         endDate,
                         rates);
      if (copied > 0) {
         int array[];
         int end = copied - 1;
         ArrayResize(array, copied);
         for(int i=end; i>=0; i--) {
            array[end-i] = (int) MathRound(rates[i].close * PRICE_RATIO);
         }
         string result_str = ANSIToUnicode(get_best(array,
                                                    ArraySize(array),
                                                    intComission));
         StringReplace(result_str, " ", "");
         string parametrs_str[];
         int count_params = StringSplit(result_str, ';', parametrs_str);
         if (count_params == 6) {
            double profit = StringToDouble(parametrs_str[5]) / PRICE_RATIO;
            if (profit > indicatorParams.profit) {
               indicatorParams.profit = profit;
               indicatorParams.fast.n = (ushort) StringToInteger(parametrs_str[0]);
               indicatorParams.fast.idMa = (ushort) StringToInteger(parametrs_str[1]);
               indicatorParams.slow.n = (ushort) StringToInteger(parametrs_str[2]);
               indicatorParams.slow.idMa = (ushort) StringToInteger(parametrs_str[3]);
               indicatorParams.strategy = (ushort) StringToInteger(parametrs_str[4]);
               indicatorParams.timeframe = timeframe;
            }
         }
      }
   }
   Print(indicatorParams.profit);
}
//i686-w64-mingw32-g++ -static-libstdc++ -static-libgcc -shared -o out.dll simple.cpp