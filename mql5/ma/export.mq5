#property copyright "Dmitry Kosolapov"
#property link      "http://www.dicos.ru"
#property version   "1.00"

const ENUM_TIMEFRAMES TF[] = {PERIOD_M5, PERIOD_M10, PERIOD_M15, PERIOD_M20};
const int COUNT_TF = ArraySize(TF);

string periodToTime(ENUM_TIMEFRAMES period)
{
   switch (period) {
      case PERIOD_M5:
         return "5";
      case PERIOD_M10:
         return "10";
      case PERIOD_M15:
         return "15";
      case PERIOD_M20:
         return "20";
      default:
         return "?";
   }
}


void OnStart()
{
   int copied=0;
   int FileHandle=0;

   MqlRates rates[];
   ArraySetAsSeries(rates,true);

   ResetLastError();

   datetime expiration_time=int(SymbolInfoInteger(_Symbol,
                                                  SYMBOL_EXPIRATION_TIME));
   datetime start_time=int(SymbolInfoInteger(_Symbol,SYMBOL_START_TIME));
   for (int pos=0; pos<COUNT_TF; pos++) {
      string FileName=Symbol() + "-" + periodToTime(TF[pos]) + ".csv";
      copied=CopyRates(Symbol(),
                       TF[pos],
                       start_time,
                       expiration_time,
                       rates);
   
      if(copied>0) {
         FileHandle=FileOpen(FileName,FILE_WRITE|FILE_ANSI);
         if(FileHandle!=INVALID_HANDLE) {
            for(int i=copied-1;i>=0;i--) {
               FileWrite(FileHandle,
                         TimeToString(rates[i].time),
                         rates[i].open,
                         rates[i].high,
                         rates[i].low,
                         rates[i].close,
                         rates[i].tick_volume,
                         rates[i].real_volume); 
           }
        }
      }
   }
   FileClose(FileHandle);
}
