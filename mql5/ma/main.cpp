#include <iostream>
#include <string.h>
#include <vector>
#include <cmath>
#ifndef TA_LIBC_H
    #include "ta_libc.h"
#endif
#include "main.hpp"


void revers_transaction(TA_Real &reversPrice,
                        const TA_Real &currPrice,
                        const directs &currDirect,
                        TA_Real &profit,
                        const TA_Real &comission) {
    if (reversPrice != 0) {
        if (currDirect == UP) {
            profit += reversPrice - currPrice - 2 * comission;
        } else {
            profit += currPrice - reversPrice - 2 * comission;
        }
    }
    reversPrice = currPrice;
}


void long_transaction(TA_Real &longPrice,
                      const TA_Real &currPrice,
                      const directs &currDirect,
                      TA_Real &profit,
                      const TA_Real &comission) {
    if (currDirect == UP) {
        longPrice = currPrice;
    } else if (longPrice > 0) {
        profit += currPrice - longPrice - 2 * comission;
    }
}


void short_transaction(TA_Real &shortPrice,
                       const TA_Real &currPrice,
                       const directs &currDirect,
                       TA_Real &profit,
                       const TA_Real &comission) {
    if (currDirect == DOWN) {
        shortPrice = currPrice;
    } else if (shortPrice > 0) {
        profit += shortPrice - currPrice - 2 * comission;
    }
}


ProfitStrategyStruct call(const std::vector <TA_Real> &prices,
                          const maInfoStruct &maInfoFast,
                          const maInfoStruct &maInfoSlow,
                          const TA_Real &comission)
{
    /* инициализация смотрим с какой промежуток по какой будем вести отсчет */
    TA_Integer startPrices = maInfoSlow.outBeg; // как будет отсчитываться цена + время
    TA_Integer indexFastMa = maInfoSlow.outBeg - maInfoFast.outBeg;
    TA_Integer indexSlowMa = 0;
    if (startPrices < maInfoFast.outBeg) {
        startPrices = maInfoFast.outBeg;
        indexFastMa = 0;
        indexSlowMa = maInfoFast.outBeg - maInfoSlow.outBeg;
    }
    TA_Integer endPrices = startPrices - 1;
    if (maInfoSlow.outNbElement > maInfoFast.outNbElement) {
        endPrices += maInfoFast.outNbElement;
    } else {
        endPrices += maInfoSlow.outNbElement;
    }
    directs prevDirect = UNDEFINED;
    directs currDirect = UNDEFINED;
    TA_Real currPrice;

    TA_Real maValueFast;
    TA_Real maValueSlow;
    TA_Real shortPrice = 0; // цена при покупки-продажи для короткой стратегии
    TA_Real longPrice = 0; // цена при покупки-продажи для длинной стратегии
    TA_Real reversPrice = 0; // цена при покупки-продажи для реверсивной стратегии
    TA_Real shortProfit = 0;
    TA_Real longProfit = 0;
    TA_Real reversProfit = 0;
    for (TA_Integer priceIndex=startPrices; priceIndex<endPrices; priceIndex++) {
        currPrice = prices[priceIndex];
        maValueFast = maInfoFast.result[indexFastMa];
        maValueSlow = maInfoSlow.result[indexSlowMa];
        if (maValueFast > maValueSlow) {
            currDirect = UP;
        } else if (maValueFast < maValueSlow) {
            currDirect = DOWN;
        } else {
            continue;
        }
        if (currDirect != prevDirect) {
            // реверсивная стратегия
            revers_transaction(reversPrice, currPrice, currDirect, reversProfit, comission);
            // длинная стратегия
            long_transaction(longPrice, currPrice, currDirect, longProfit, comission);
            // короткая стратегия
            short_transaction(shortPrice, currPrice, currDirect, shortProfit, comission);
        }
        prevDirect = currDirect;
        indexFastMa++;
        indexSlowMa++;
    }
    ProfitStrategyStruct result = {REVERS, reversProfit};
    if (reversProfit < longProfit) {
        result.strategy = LONG;
        result.profit = longProfit;
    } else if (reversProfit < shortProfit) {
        result.strategy = SHORT;
        result.profit = shortProfit;
    }
    return result;
}


BestStruct get_ma_values(const std::vector <TA_Real> &prices, const TA_Real &comission)
{
    //TODO: сделать единократный расчет скользящих средних, чтобы не считать их каждый раз
    using namespace std;
    unsigned int endPrice = prices.size();
    TA_Real tmpArray[endPrice];
    maInfoStruct maInfo[COUNT_MA_VALUES];
    TA_RetCode retCode;
    unsigned int endMaCall = 0;
    /* расчитываем заранее скользящие средние */
    TA_Integer outNbElement = 0;
    TA_Integer outBeg = 0;
    TA_Integer endIndx = endPrice - 1;
    for (unsigned short func_num=0; func_num<COUNT_MA; func_num++) {
        for (unsigned short n=N_MIN; n<=N_MAX; n++) {
            retCode = standartFuncs[func_num](0,
                                    endIndx,
                                    &prices[0],
                                    n,
                                    &outBeg,
                                    &outNbElement,
                                    &tmpArray[0]);
            if(retCode != TA_SUCCESS) {
                continue;
            }
            maInfo[endMaCall].outBeg = outBeg;
            maInfo[endMaCall].outNbElement = outNbElement;
            maInfo[endMaCall].n = n;
            maInfo[endMaCall].idMa = func_num;
            for (TA_Integer pos=0; pos<maInfo[endMaCall].outNbElement; pos++) {
                maInfo[endMaCall].result.push_back(tmpArray[pos]);
            }
            endMaCall++;
        }
    }
    ProfitStrategyStruct current;
    TA_Real currentProfit = -100000000; // просто любое отрицательное число
    BestStruct result;
    for (unsigned short fastIndex=0; fastIndex<endMaCall; fastIndex++) {
        for (unsigned short slowIndex=0; slowIndex<endMaCall; slowIndex++) {
            if ((maInfo[slowIndex].n-10) > maInfo[fastIndex].n) {
                current = call(prices,
                               maInfo[fastIndex],
                               maInfo[slowIndex],
                               comission);
                if (current.profit > currentProfit) {
                    currentProfit = current.profit;
                    result.fast.n = maInfo[fastIndex].n;
                    result.fast.idMa = maInfo[fastIndex].idMa;
                    result.slow.n = maInfo[slowIndex].n;
                    result.slow.idMa = maInfo[slowIndex].idMa;
                    result.profit = current.profit;
                    result.strategy = current.strategy;
                }
            }
        }
    }
    return result;
}


void taInitialize()
{
    TA_RetCode retCode = TA_Initialize();
    if(retCode != TA_SUCCESS) {
        char errorTemplate[] = "Cannot initialize TA-Lib (%d)!\n";
        char error[200];
        sprintf(error, errorTemplate, retCode);
        throw error;
    }
    standartFuncs[0] = TA_SMA;
    standartFuncs[1] = TA_WMA;
    standartFuncs[2] = TA_EMA;
    standartFuncs[3] = TA_DEMA;
    standartFuncs[4] = TA_TEMA;
    standartFuncs[5] = TA_TRIMA;
    standartFuncs[6] = TA_KAMA;
}


void taShutdown()
{
    TA_Shutdown();
}


inline std::vector <TA_Real> normalizePrices(const TA_Integer prices[], const int &count)
{
    std::vector <TA_Real> prices_vec;
    TA_Real currPrice;
    for (int i=0; i<count; i++) {
        currPrice = ((TA_Real) prices[i] / PRICES_RATIO);
        prices_vec.push_back(currPrice);
    }
    return prices_vec;
}


BestStruct get_best_system(const TA_Integer prices[], unsigned int count, TA_Real comission)
{
    taInitialize();
    std::vector <TA_Real> prices_vec = normalizePrices(prices, count);
    BestStruct result = get_ma_values(prices_vec, comission);
    taShutdown();
    return result;
}
/*

int main()
{
    TA_Real prices[] = {1,2,3,4,5,6,7,8,9,0,1,200,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0};
    BestStruct result = get_best_system(prices, 30, 1);
    std::cout << result.profit << std::endl;
    return 0;
}*/


extern "C" {
    int call_func(const int n, const int idMa, const int prices_int[], const int count)
    {
        if (idMa >= COUNT_MA || idMa < 0) {
            throw "скользящей средней с таким id не существует";
        }
        taInitialize();
        std::vector <TA_Real> prices_vec = normalizePrices(prices_int, count);
        TA_Real tmpArray[count] = {0};
        TA_Integer outNbElement = 0;
        TA_Integer outBeg = 0;
        TA_RetCode retCode;
        TA_Integer endIndx = count - 1;
        retCode = standartFuncs[idMa](0,
                                      endIndx,
                                      &prices_vec[0],
                                      n,
                                      &outBeg,
                                      &outNbElement,
                                      &tmpArray[0]);
        taShutdown();
        if(retCode != TA_SUCCESS) {
            char errorTemplate[] = "Cannot call TA-Lib (%d)!\n";
            char error[200];
            sprintf(error, errorTemplate, retCode);
            throw error;
        }
        return (int) (tmpArray[outNbElement-1] * PRICES_RATIO);
    }

    const char * get_best(const int prices_int[], unsigned int count, int intComission)
    {
        using namespace std;
        double comission = ((double) intComission) / COMISSION_RATIO;
        auto result = get_best_system(prices_int, count, comission);
        int profit = (int) round(result.profit * PRICES_RATIO);
        string str_result = to_string(result.fast.n) + \
                            ";" + to_string(result.fast.idMa) + \
                            ";" + to_string(result.slow.n) + \
                            ";" + to_string(result.slow.idMa) + \
                            ";" + to_string(result.strategy) + \
                            ";" + to_string(profit);
        return str_result.c_str();
    }
}
