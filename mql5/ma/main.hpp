const TA_Real COMISSION_RATIO = 100;
const TA_Real PRICES_RATIO = 1000;
const unsigned short N_MIN = 10;
const unsigned short N_MAX = 200;
const unsigned short COUNT_MA = 7;
const unsigned int COUNT_MA_VALUES = (N_MAX - N_MIN + 1) * COUNT_MA; // полное количество всех скользящих средних


enum strategies {LONG, SHORT, REVERS, END_STRATEGIES};
enum directs {UNDEFINED, UP, DOWN};

const unsigned int MIN_PROFIT = 0;


struct ProfitStrategyStruct {
    strategies strategy;
    TA_Real profit;
};


struct maInfoStruct {
    TA_Integer outNbElement;
    std::vector<TA_Real> result; //tmpArr
    TA_Integer outBeg;
    unsigned short n;
    unsigned short idMa;
};


TA_RetCode (*standartFuncs[COUNT_MA])(TA_Integer,
                                      TA_Integer,
                                      const TA_Real[],
                                      TA_Integer,
                                      TA_Integer *,
                                      TA_Integer *,
                                      TA_Real[]);


struct MaParamsStruct {
    unsigned short n;
    unsigned short idMa;
};


struct BestStruct {
    MaParamsStruct fast;
    MaParamsStruct slow;
    strategies strategy;
    TA_Real profit;
};
