-- тут будет временная информация об sql коде

COPY mafastmaslow FROM '/home/dmitry/web/trading/trading/others/SBER-9.13_full.csv' DELIMITER ',' CSV; -- копирование из файла в таблицу

-- Table: mafastmaslow
-- DROP TABLE mafastmaslow;
CREATE TABLE mafastmaslow
(
  date date,
  profit int,
  nfast smallint,
  idfastma smallint,
  nslow smallint,
  idslowma smallint,
  timeframe smallint,
  strategy smallint
)
WITH (
  OIDS=FALSE
);
ALTER TABLE mafastmaslow
  OWNER TO postgres;
-- Index: date_inx
-- DROP INDEX date_inx;
CREATE INDEX date_inx
  ON mafastmaslow
  USING btree
  (date);
-- Index: idfastma_inx
-- DROP INDEX idfastma_inx;
CREATE INDEX idfastma_inx
  ON mafastmaslow
  USING btree
  (idfastma);
-- Index: idslowma_inx
-- DROP INDEX idslowma_inx;
CREATE INDEX idslowma_inx
  ON mafastmaslow
  USING btree
  (idslowma);
-- Index: nfast_inx
-- DROP INDEX nfast_inx;
CREATE INDEX nfast_inx
  ON mafastmaslow
  USING btree
  (nfast);
-- Index: nslow_inx
-- DROP INDEX nslow_inx;
CREATE INDEX nslow_inx
  ON mafastmaslow
  USING btree
  (nslow);
-- Index: timeframe_inx
-- DROP INDEX timeframe_inx;
CREATE INDEX timeframe_inx
  ON mafastmaslow
  USING btree
  (timeframe);


-- поиск лучшей стратегии
SELECT AVG(profit) as avgprof, nfast, nslow, timeframe, strategy
  FROM mafastmaslow
WHERE "date" > '2013-08-22' and "date" <= '2013-08-26'
and timeframe<30 AND timeframe > 5  AND 
    (nslow - nfast) >= 10
GROUP BY nfast, nslow, timeframe, strategy
HAVING AVG(profit) > 0
ORDER BY avgprof DESC, nfast, nslow, timeframe, strategy
LIMIT 20;

-- проверка стратегии
SELECT SUM(profit) as prof, nfast, nslow, timeframe, strategy,"date"
  FROM mafastmaslow
WHERE "date" = '2013-08-22'
    AND nfast = 156 AND nslow=181 AND timeframe=10 AND strategy=1
GROUP BY nfast, nslow, timeframe, strategy, "date"
LIMIT 20;


-- функция для получения лучшей прибыли
-- create extension plpython3u;  -- Для постгреса 9.3 питон 3.2
-- CREATE LANGUAGE plpython2u;
CREATE TYPE profit_struct AS (
  profit integer,
  "date" date,
  nfast smallint,
  idfastma smallint,
  nslow smallint,
  idslowma smallint,
  timeframe smallint,
  strategy smallint
);

DROP FUNCTION IF EXISTS get_profit();
CREATE FUNCTION get_profit ()
  RETURNS SETOF profit_struct
AS $$
  from datetime import timedelta, datetime
  min_date_sql = plpy.prepare("""SELECT MIN("date") AS min_date FROM mafastmaslow""")
  plpy.debug(plpy.execute(min_date_sql)[0])
  min_date_str = plpy.execute(min_date_sql)[0]['min_date']
  min_date = datetime.strptime(min_date_str, "%Y-%m-%d")
  max_date_sql = plpy.prepare("""SELECT MAX("date") AS max_date FROM mafastmaslow""")
  max_date_str = plpy.execute(max_date_sql)[0]['max_date']
  max_date = datetime.strptime(max_date_str, "%Y-%m-%d")
  curr_date = min_date + timedelta(days=8)
  date_start = min_date
  date_end = min_date + timedelta(days=7)
  one_day = timedelta(days=1)
  blank_structure = (0, datetime.now(), -1, -1, -1, -1, 0, -1,)
  find_best_sql = plpy.prepare("""SELECT 
      SUM(profit) as sumprof, nfast, idfastma, nslow, idslowma, timeframe, strategy
      FROM mafastmaslow
      WHERE "date" between $1 and $2
      GROUP BY nfast, idfastma, nslow, idslowma, timeframe, strategy
      HAVING SUM(profit) > 0
      ORDER BY sumprof DESC, nfast, nslow, timeframe""", ["date", "date"])
  current_profit_sql = plpy.prepare("""SELECT profit
      FROM mafastmaslow
      WHERE "date"=$1 AND nfast=$2 AND idfastma=$3 AND nslow=$4 and 
             idslowma=$5 and timeframe=$6 and strategy=$7""",
      ["date", "integer", "integer", "integer", "integer", "integer", "integer"])
  while curr_date <= max_date:
    curr_struct = blank_structure
    profit_params = plpy.execute(find_best_sql, [date_start, date_end], 1)
    if profit_params.nrows() > 0:
      old_profit = profit_params[0]
      result = plpy.execute(current_profit_sql,
                            [curr_date, old_profit['nfast'], old_profit['idfastma'],
                             old_profit['nslow'], old_profit['idslowma'],
                             old_profit['timeframe'], old_profit['strategy']])
      if result.nrows() > 0:
        curr_struct =  [result[0]['profit'], curr_date, old_profit['nfast'],
                        old_profit['idfastma'], old_profit['nslow'],
                        old_profit['idslowma'], old_profit['timeframe'],
                        old_profit['strategy']]
    yield curr_struct
    date_start += one_day
    date_end += one_day
    curr_date += one_day
$$ LANGUAGE plpython2u;

-- Данные для тестирования:
INSERT INTO mafastmaslow(
    date, profit, nfast, idfastma, nslow, idslowma, timeframe, strategy)
VALUES ('2014-01-01', 100, 90, 160, 150, 4, 20, 0),
('2014-01-02', 100, 90, 160, 150, 4, 20, 0),
('2014-01-03', 100, 90, 160, 150, 4, 20, 0),
('2014-01-04', 100, 90, 160, 150, 4, 20, 0),
('2014-01-05', 100, 90, 160, 150, 4, 20, 0),
('2014-01-06', 100, 90, 160, 150, 4, 20, 0),
('2014-01-07', 100, 90, 160, 150, 4, 20, 0),
('2014-01-08', 100, 90, 160, 150, 4, 20, 0),
('2014-01-09', 100, 90, 160, 150, 4, 20, 0),
('2014-01-10', 100, 90, 160, 150, 4, 20, 0),
('2014-01-11', 100, 90, 160, 150, 4, 20, 0),
('2014-01-12', 100, 90, 160, 150, 4, 20, 0),
('2014-01-13', 100, 90, 160, 150, 4, 20, 0),
('2014-01-14', 100, 90, 160, 150, 4, 20, 0),
('2014-01-01', -100, 91, 160, 150, 4, 20, 0),
('2014-01-02', -100, 91, 160, 150, 4, 20, 0),
('2014-01-03', -100, 91, 160, 150, 4, 20, 0),
('2014-01-04', -100, 91, 160, 150, 4, 20, 0),
('2014-01-05', -100, 91, 160, 150, 4, 20, 0),
('2014-01-06', -100, 91, 160, 150, 4, 20, 0),
('2014-01-07', -100, 91, 160, 150, 4, 20, 0),
('2014-01-08', -100, 91, 160, 150, 4, 20, 0),
('2014-01-09', -100, 91, 160, 150, 4, 20, 0),
('2014-01-10', -100, 91, 160, 150, 4, 20, 0),
('2014-01-11', -100, 91, 160, 150, 4, 20, 0),
('2014-01-12', -100, 91, 160, 150, 4, 20, 0),
('2014-01-13', -100, 91, 160, 150, 4, 20, 0),
('2014-01-14', -100, 91, 160, 150, 4, 20, 0)

-- как работать с функцией:
SELECT * FROM get_profit();
