# coding: utf8
import time as sl_time
import csv
import os
import sys
import subprocess
from datetime import timedelta, datetime, date, time
from collections import deque

import consts
from csv_connector import CsvConnector
from bar import combine_bars, Bar
from utils import get_minutes


if len(sys.argv) != 2:
    print("Нужно передавать название бумаги")
    sys.exit(1)


start_periods = {}


class SaveCSVBar(object):
    """ Сохранеяет бары в csv файлы """

    def __init__(self, autocommit=True, period=10000):
        self.autocommit = autocommit
        self.period = period
        self.count = 0
        self.data = {} # ключ -- имя файла, значение -- кортеджы с барами
        self.files = {}  # ключ - имя файла, значение -- csv ресурс файла

    def commit(self):
        if self.count > 0:
            for filename, data in self.data.items():
                self.files[filename].writerows(data)
            self.data = {}
            self.count = 0

    def __call__(self, bar, paper_no, timeframe):
        minutes = get_minutes(timeframe)
        filename = "../data/%s_%s.csv" % (paper_no, minutes)
        if filename not in self.files:
            self.files[filename] = csv.writer(open(filename, 'w'))
        if filename not in self.data:
            self.data[filename] = []
        info = (bar.time_start.strftime("%Y-%m-%d %H:%M:%S"),
                bar.close,
                bar.volume,)
        self.data[filename].append(info)
        self.count += 1
        if self.autocommit and self.count == self.period:
            self.commit()


def get_by_timeframes(bar, paper_no, logger=None):
    """ формирует бары заданных таймфреймов consts.TIMEFRAMES 
        относительно предыдущих минутных баров
        Не возвращает и не сохраняет таймфремы за последний бар.
        Вывод нужно делать самостоятельно"""
    global start_periods
    bars = {}
    rm_sec = timedelta(seconds=bar.time_start.second)
    if paper_no not in start_periods:
        start_periods[paper_no] = {}
        for timeframe in consts.TIMEFRAMES:
            values = {'time_start': bar.time_start - rm_sec,
                      'bars': [bar]}
            start_periods[paper_no][timeframe] = values
        return bars

    for timeframe in consts.TIMEFRAMES:
        b_time_start = bar.time_start - rm_sec
        next_time = b_time_start - timeframe
        info = start_periods[paper_no][timeframe]
        if next_time >= info['time_start']:
            c_bar = combine_bars(info['bars'])
            if logger is not None:
                logger(c_bar, paper_no, timeframe)
            bars[timeframe] = c_bar
            start_periods[paper_no][timeframe] = {'time_start': b_time_start,
                                                  'bars': [bar]}
        else:
            start_periods[paper_no][timeframe]['bars'].append(bar)
    return bars

def generate_bars(connector, bar_saver=CsvConnector):
    bars = connector.get_data()
    save_bar = bar_saver()
    while bars is not None:
        for paper_no, bar in bars.items():
            get_by_timeframes(bar, paper_no, logger=save_bar)
        bars = connector.get_data()
    save_bar.commit()


connector = CsvConnector('%s.csv' % sys.argv[1])
generate_bars(connector, SaveCSVBar)
del start_periods
subprocess.call(("cd ..; ./a.out %s" % sys.argv[1]), shell=True)
