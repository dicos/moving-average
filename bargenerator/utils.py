# coding: utf8
from decimal import Decimal


def get_minutes(timeframe):
    """ преобразовывает таймфрем в количество минут. """
    return round(timeframe.seconds / 60)


def to_kop(price):
    """ преобразовывает рубли в копейки """
    return int(price * 100) if price else price


def to_rub(price):
    """ Преобразовывает копейки в рублевую и копеечную части """
    return Decimal(price / 100) if price else price
