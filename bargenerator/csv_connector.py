# coding: utf8
from datetime import datetime, timedelta
from collections import deque
import csv
import sys

from bar import Bar
from utils import to_rub, to_kop


class CsvConnector(object):
    """ Параметры csv файла:
        0 номер бумаги,
        1 количество,
        2 цена,
        3 датавремя,
        4 направление движения цены (непонятно),
        5 тип сделки,
        6 название бумаги (текст) """

    bid_number = 0

    def __init__(self, csv_path='../../all_trade.csv'):
        self.prev_bars_info = deque(maxlen=3)
        self.bids = {}
        self._curr_time = None
        self.next_info = {}
        self.paper_period = {}
        self.csv_path = csv_path
        self.connect()

    def connect(self): 
        self.file_res = open(self.csv_path)
        self.reader = csv.reader(self.file_res)

    def __getstate__(self):
        state = self.__dict__.copy()
        for name in 'file_res', 'reader':
            del state[name]
        return state

    def disconnect(self):
        pass

    def get_curr_time(self):
        """ Текущее время """

        if self._curr_time is None:
            return None
        else:
            self._curr_time += timedelta(minutes=1)
            return self._curr_time

    def set_curr_time(self, curr_datetime):
        curr_datetime -= timedelta(seconds=curr_datetime.second)
        self._curr_time = curr_datetime

    def get_sample_info(self):
        return {'max': 0,
                'min': sys.maxsize,
                'open': 0,
                'close': 0,
                'volume': 0}

    def from_next_bars_data(self):
        data = {}
        # Кеширование
        if len(self.next_info) > 0:
            for paper_no, info in self.next_info.items():
                paper_no, price, volume, t_time = info
                data[paper_no] = self.get_sample_info()
                data[paper_no]['open'] = price
                data[paper_no]['time_start'] = t_time
                data[paper_no]['max'] = price
                data[paper_no]['min'] = price
                data[paper_no]['close'] = price
                data[paper_no]['volume'] = volume
                data[paper_no]['time_end'] = t_time
                self.paper_period[paper_no] = 0
            self.next_info = {}
        return data

    def get_row_data(self, row):
        t_time = datetime.strptime(row[0], "%Y-%m-%d %H:%M:%S")
        price = to_kop(int(row[1]))
        volume = int(row[2])
        paper_no = row[3]
        return t_time, price, volume, paper_no

    def to_bar(self, data):
        return dict([(num, Bar(v)) for bum, v in data.items()])

    def get_data(self, papers_no=None, is_update_time=True):
        bars = {}
        count_prev_bars = len(self.prev_bars_info)
        if not is_update_time:
            if count_prev_bars > 0:
                if papers_no is not None:
                    for paper_no in papers_no:
                        bars_info = self.prev_bars_info[0]
                        if paper_no in bars_info:
                            bars[paper_no] = bars_info[paper_no]
                    return bars
                else:
                    return self.prev_bars_info[0]
            else:
                return bars

        curr_time = self.get_curr_time()

        data = {}
        if self.next_info:
            if tuple(self.next_info.values())[0][3] < curr_time:
                data = self.from_next_bars_data()
            else:
                return data
            
        for row in self.reader:
            t_time, price, volume, paper_no  = self.get_row_data(row)
            if curr_time is None:
                self.set_curr_time(t_time)
                curr_time = self.get_curr_time()
            elif curr_time <= t_time:
                self.next_info = {paper_no: (paper_no, price, volume, t_time,)}
                break
            if paper_no not in data:
                data[paper_no] = self.get_sample_info()
                data[paper_no]['open'] = price
                data[paper_no]['time_start'] = t_time
            if data[paper_no]['max'] < price:
                data[paper_no]['max'] = price
            if data[paper_no]['min'] > price:
                data[paper_no]['min'] = price
            data[paper_no]['close'] = price
            data[paper_no]['volume'] += volume
            data[paper_no]['time_end'] = t_time
    
        bars = {}
        for paper_no, info in data.items():
            bars[paper_no] = Bar(info)
        self.prev_bars_info.appendleft(bars)
        if len(bars) == 0:
            self.file_res.close()
            del self.file_res
            self.reader = tuple()
            return None
        if papers_no is None:
            return bars
        else:
            return dict([(k,v) for k, v in bars.items() if k in papers_no])

