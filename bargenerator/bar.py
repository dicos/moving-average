# coding: utf8


def combine_bars(bars):
    """ Делает из списка баров 1 бар """
    bar_data = {'max': bars[0].max,
                'min': bars[0].min,
                'open': bars[0].open,
                'close': bars[0].close,
                'volume': bars[0].volume,
                'time_start': bars[0].time_start,
                'last_time_bar': bars[0].time_start}
    for bar in bars[1:]:
        if bar.time_start < bar_data['time_start']:
            bar_data.update({'time_start': bar.time_start,
                             'open': bar.open})
        elif bar.time_start > bar_data['last_time_bar']:
            bar_data.update({'last_time_bar': bar.time_start,
                             'close': bar.close})
        if bar.max > bar_data['max']:
            bar_data['max'] = bar.max
        if bar.min < bar_data['min']:
            bar_data[min] = bar.min
        bar_data['volume'] += bar.volume
    return Bar(bar_data)


class Bar(object):
    """ Содержит информацию о баре.
        Преимущество хранения информации о баре в объекте перед словарем в том,
        чтобы невозможно изменить случайно какой-нибудь параметр бара """
    __slots__ = ('_max', '_min', '_open', '_close', '_volume', '_time_start',)

    def __init__(self, bar_dict):
        self._max = int(bar_dict['max'])
        self._min = int(bar_dict['min'])
        self._open = int(bar_dict['open'])
        self._close = int(bar_dict['close'])
        self._volume = int(bar_dict['volume'])
        self._time_start = bar_dict['time_start']

    def __eq__(self, other):
        params = ('max', 'min', 'open', 'close', 'volume', 'time_start',)
        for param in params:
            if getattr(self, param) != getattr(other, param):
                return False
        return True

    def get_bar_info(self):
        return {'max': self.max,
                'min': self.min,
                'open': self.open,
                'close': self.close,
                'volume': self.volume,
                'time_start': self.time_start}

    @property
    def max(self):
        return self._max

    @property
    def min(self):
        return self._min

    @property
    def open(self):
        return self._open

    @property
    def close(self):
        return self._close

    @property
    def volume(self):
        return self._volume

    @property
    def time_start(self):
        return self._time_start
