#coding: utf8
from datetime import timedelta


TIMEFRAMES = [timedelta(minutes=i) for i in (5, 10, 15, 20,)]
